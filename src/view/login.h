#ifndef LOGIN_H
#define LOGIN_H

#include <QWidget>
#include <QPushButton>
#include <QLineEdit>
#include <QLabel>

namespace Ui {
    class Login;
}

class Login : public QWidget
{
    Q_OBJECT

private:
    Ui::Login * ui;

public:
    explicit Login(QWidget *parent = 0);
    ~Login();

    bool keepLogin();
    QPushButton * btnLogin();
    QPushButton * btnSignUp();
    QLineEdit   * lineEditEmail();
    QLineEdit   * lineEditPassword();
    QLabel      * labelLoginError();

protected:
    void initUi();

private slots:

signals:

public slots:
};

#endif // LOGIN_H
