#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QCloseEvent>

namespace Ui {
    class MainWindow;
}

class MainLogin;
class MainWindow : public QMainWindow
{
    Q_OBJECT
private:
    Ui::MainWindow * ui;
    MainLogin   * m_mainLoginWidget = nullptr;

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

    MainLogin * mainLoginWidget();
    void closeEvent(QCloseEvent * a_event);


private:
    bool closeMainLogin();
    void getConnections();
    void showWidget(QWidget * a_widget, bool a_isModal = false);

private slots:

signals:

public slots:
};

#endif // MAINWINDOW_H
