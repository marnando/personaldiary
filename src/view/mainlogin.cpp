#include "ui_mainlogin.h"
#include "mainlogin.h"
#include "../view/login.h"
#include "../view/signup.h"
#include <QPushButton>

MainLogin::MainLogin(QWidget *parent) : QWidget(parent),
    m_loginWidget(new Login),
    m_signUpWidget(new SignUp),
    ui(new Ui::MainLogin)
{
    ui->setupUi(this);
    initUi();
}

MainLogin::~MainLogin()
{
    delete ui;
}

bool MainLogin::closeWidget()
{
    if (m_signUpWidget->isVisible() == true)
    {
        showLogin();
        return false;
    }

    return true;
}

void MainLogin::closeEvent(QCloseEvent * a_event)
{
    a_event->ignore();
    if (m_signUpWidget->isVisible() == true)
    {
        showLogin();
        return;
    }
    a_event->accept();
    QWidget::closeEvent(a_event);
}

void MainLogin::initUi()
{
    layout()->addWidget(m_loginWidget);
    layout()->addWidget(m_signUpWidget);
    m_signUpWidget->hide();
    m_loginWidget->show();
    getConnections();
}

void MainLogin::getConnections()
{
    connect(m_loginWidget->btnSignUp(), SIGNAL(clicked(bool)), SLOT(makeRegister()));
}

void MainLogin::makeRegister()
{
    m_loginWidget->hide();
    m_signUpWidget->show();
}

void MainLogin::showLogin()
{
    m_signUpWidget->hide();
    m_loginWidget->show();
}
