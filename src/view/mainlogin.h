#ifndef MAINLOGIN_H
#define MAINLOGIN_H

#include <QWidget>
#include <QCloseEvent>

namespace Ui {
    class MainLogin;
}

class Login;
class SignUp;
class MainLogin : public QWidget
{
    Q_OBJECT

private:
    Ui::MainLogin * ui;
    Login   * m_loginWidget = nullptr;
    SignUp  * m_signUpWidget = nullptr;

public:
    explicit MainLogin(QWidget *parent = 0);
    ~MainLogin();

    bool closeWidget();
    void closeEvent(QCloseEvent * a_event);

protected:
    void initUi();
    void getConnections();

signals:

public slots:
    void makeRegister();
    void showLogin();
};

#endif // MAINLOGIN_H
