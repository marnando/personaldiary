#include "ui_signup.h"
#include "signup.h"

SignUp::SignUp(QWidget *parent) : QWidget(parent),
    ui(new Ui::SignUp)
{
    ui->setupUi(this);
}

SignUp::~SignUp()
{
    delete ui;
}

QLineEdit *SignUp::nickNameLineEdit()
{
    return ui->m_leNicknameReg;
}

QLineEdit *SignUp::emailLineEdit()
{
    return ui->m_leEmailReg;
}

QLineEdit *SignUp::passwordLineEdit()
{
    return ui->m_lePasswordReg;
}

QPushButton *SignUp::registerButton()
{
    return ui->m_btnRegister;
}
