#include "ui_mainwindow.h"
#include "../utils/widgetutils.h"
#include "mainwindow.h"
#include "mainlogin.h"
#include <QVBoxLayout>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
  m_mainLoginWidget(new MainLogin),
  ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    getConnections();
    setCentralWidget(m_mainLoginWidget);
    WidgetUtils::instance()->showWidget(*m_mainLoginWidget);
}

MainWindow::~MainWindow()
{
    delete ui;
}

MainLogin * MainWindow::mainLoginWidget()
{
    return m_mainLoginWidget;
}

void MainWindow::closeEvent(QCloseEvent *a_event)
{
    a_event->ignore();
    if (m_mainLoginWidget->isVisible() == true)
    {
        if (closeMainLogin() == true)
        {
            a_event->accept();
            QMainWindow::closeEvent(a_event);
        }
    }
}

bool MainWindow::closeMainLogin()
{
    return m_mainLoginWidget->closeWidget();
}

void MainWindow::getConnections()
{

}
