#include "ui_login.h"
#include "login.h"

Login::Login(QWidget *parent) : QWidget(parent),
    ui(new Ui::Login)
{
    ui->setupUi(this);
    initUi();
}

Login::~Login()
{
    delete ui;
}

bool Login::keepLogin()
{
    return ui->m_cbxKeepLogin->isChecked();
}

QPushButton *Login::btnLogin()
{
    return ui->m_btnLogin;
}

QPushButton *Login::btnSignUp()
{
    return ui->m_btnSingUp;
}

QLineEdit *Login::lineEditEmail()
{
    return ui->m_leEmail;
}

QLineEdit *Login::lineEditPassword()
{
    return ui->m_lePassword;
}

QLabel *Login::labelLoginError()
{
    return ui->m_lblError;
}

void Login::initUi()
{
    QPalette palette = ui->m_lblError->palette();
    palette.setColor(ui->m_lblError->foregroundRole(), Qt::red);
    ui->m_lblError->setPalette(palette);
}
