#ifndef SIGNUP_H
#define SIGNUP_H

#include <QWidget>
#include <QLineEdit>
#include <QPushButton>

namespace Ui {
    class SignUp;
}

class SignUp : public QWidget
{
    Q_OBJECT

private:
    Ui::SignUp * ui;

public:
    explicit SignUp(QWidget *parent = 0);
    ~SignUp();

    QLineEdit * nickNameLineEdit();
    QLineEdit * emailLineEdit();
    QLineEdit * passwordLineEdit();
    QPushButton * registerButton();



signals:

public slots:
};

#endif // SIGNUP_H
