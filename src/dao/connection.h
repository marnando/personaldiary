#ifndef CONNECTION_H
#define CONNECTION_H

#include <QObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlQueryModel>
#include <QStandardItemModel>

class Connection : public QObject
{
    Q_OBJECT

public:
    static Connection * m_connection;

private:
    QSqlDatabase * m_db;

public:
    explicit Connection(QObject *parent = 0);
    ~Connection();

    static void createConnection();
    bool createConnectionDb();
    static Connection * instance();

    QSqlDatabase * instanceDB();
    void releaseDB();

    int getNextId();
    bool insert(QString a_table, QList<QVariant> a_listOfData, QList<QString>  a_listOfBind);
    QList<QList<QVariant> > selectAll(QString a_table, QList<QVariant> a_listOfData, QList<QString>  a_listOfBind);

signals:

public slots:
};

#endif // CONNECTION_H
