#ifndef WIDGETUTILS_H
#define WIDGETUTILS_H

#include <QWidget>
#include <QList>

class WidgetUtils : public QWidget
{
private:
    static WidgetUtils * util;
    QList<QWidget *> m_listOfWidgets = QList<QWidget *>();

public:
    WidgetUtils();
    ~WidgetUtils();

    void showWidget(QWidget & a_widget, bool a_isModal = false);
    static WidgetUtils * instance();
    static void release();

private:
    void setModality(QWidget & a_widget, bool a_isModal = false);
    void addWidgetOnlist(QWidget *a_widget);
    void onlyAddWidgetOnList(QWidget *a_widget);
};

#endif // WIDGETUTILS_H
