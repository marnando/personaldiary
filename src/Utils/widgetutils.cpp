#include "widgetutils.h"

WidgetUtils * WidgetUtils::util = nullptr;
WidgetUtils::WidgetUtils()
{
}

WidgetUtils::~WidgetUtils()
{
    release();
}

void WidgetUtils::showWidget(QWidget & a_widget, bool a_isModal)
{
    if (m_listOfWidgets.isEmpty() == true)
    {
        onlyAddWidgetOnList(&a_widget);
        setModality(a_widget, a_isModal);
        a_widget.show();
        return;
    }

    foreach (QWidget * wid, m_listOfWidgets) {
        wid->hide();
        if (wid->objectName() == a_widget.objectName())
        {
            setModality(*wid, a_isModal);
        } else
        {
            onlyAddWidgetOnList(wid);
        }
        wid->show();
    }
}

WidgetUtils * WidgetUtils::instance()
{
    if (util == nullptr)
    {
        util = new WidgetUtils();
    }

    return util;
}

void WidgetUtils::release()
{
    if (util != nullptr)
    {
        delete util;
        util = nullptr;
    }
}

void WidgetUtils::setModality(QWidget & a_widget, bool a_isModal)
{
    if (a_widget.isModal() == true)
    {
        a_widget.setWindowFlags(Qt::Widget);
        a_widget.setWindowModality(Qt::NonModal);
    }

    if (a_isModal == true)
    {
        a_widget.setWindowFlags(Qt::Dialog);
        a_widget.setWindowModality(Qt::WindowModal);
    }
}

void WidgetUtils::addWidgetOnlist(QWidget * a_widget)
{
    bool founded = false;
    foreach (QWidget * wid, m_listOfWidgets) {
        if (wid->objectName() == a_widget->objectName())
        {
            founded = true;
            break;
        }
    }

    if (founded == false)
    {
        m_listOfWidgets << a_widget;
    }
}

void WidgetUtils::onlyAddWidgetOnList(QWidget * a_widget)
{
    m_listOfWidgets << a_widget;
}
