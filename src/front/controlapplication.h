#ifndef CONTROLAPPLICATION_H
#define CONTROLAPPLICATION_H

#include <QObject>

class MainWindow;
class ControlApplication : public QObject
{
    Q_OBJECT
private:
    MainWindow * m_mainWindow = nullptr;

public:
    explicit ControlApplication(QObject *parent = 0);
    ~ControlApplication();

    void init();

signals:

public slots:
};

#endif // CONTROLAPPLICATION_H
