/**
    ControlApplication is a front controller responsable to comunicate MainWindow from view folder with
    controls classes from control folder from MVC.
*/
#include "controlapplication.h"
#include "../view/mainwindow.h"
#include <QApplication>
#include <QDesktopWidget>
#include <qsystemdetection.h>
#include <QDebug>

ControlApplication::ControlApplication(QObject *parent) : QObject(parent)
{
}

ControlApplication::~ControlApplication()
{
    if (m_mainWindow != nullptr) { delete m_mainWindow; m_mainWindow = nullptr; }
}

void ControlApplication::init()
{
    if (m_mainWindow == nullptr)
    {
        m_mainWindow = new MainWindow();
    }

#if defined(Q_OS_WIN)
    qDebug() << "Windows SO";
    QDesktopWidget *desktop = qApp->desktop();
    int height = desktop->height();
    int width = desktop->width();
    int newHeigth = 0;
    int newWidth = 0;

    //80% of the height and width
    newHeigth = height * 0.8;
    newWidth = width * 0.8;

    m_mainWindow->setFixedSize(newWidth, newHeigth);
    m_mainWindow->show();
#elif defined(Q_OS_ANDROID)
    qDebug() << "Android SO";
    m_mainWindow->showFullScreen();
#endif
}

