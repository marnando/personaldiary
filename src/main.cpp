#include "src/front/controlapplication.h"
#include <QApplication>
#include <QDesktopWidget>
#include <qsystemdetection.h>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    ControlApplication control;
    control.init();
    return a.exec();
}


