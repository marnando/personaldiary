#-------------------------------------------------
#
# Project created by QtCreator 2015-06-22T21:33:53
#
#-------------------------------------------------

QT       += core widgets gui sql

TARGET = PersonalDiary
TEMPLATE = app
QMAKE_CXXFLAGS += -std=c++0x

# addFiles.sources = qrc/database/healthup_database.db3/

HEADERS += \
    src/view/mainwindow.h \
    src/view/login.h \
    src/view/signup.h \
    src/view/mainlogin.h \
    src/utils/widgetutils.h \
    src/front/controlapplication.h

SOURCES += \
    src/view/mainwindow.cpp \
    src/main.cpp \
    src/view/login.cpp \
    src/view/signup.cpp \
    src/view/mainlogin.cpp \
    src/utils/widgetutils.cpp \
    src/front/controlapplication.cpp

FORMS += \
    qrc/ui/mainwindow.ui \
    qrc/ui/login.ui \
    qrc/ui/signup.ui \
    qrc/ui/mainlogin.ui

RESOURCES += \
    qrc/resources.qrc

